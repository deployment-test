Capistrano::Configuration.instance(true).load do |configuration|
  before "deploy:update_code", "git:calculate_tag"

  namespace :git do
    def sh cmd
      puts cmd; system cmd

      if $? != 0
        raise "#{cmd} failed"
      end
    end

    def last_tag_matching(pattern)
      lastTag = nil

      matching_tags = `git tag -l '#{pattern}'`
      matching_tags = matching_tags.split
      natcmp_src = File.join(File.dirname(__FILE__), '/natcmp.rb')
      require natcmp_src
      matching_tags.sort! do |a,b|
        String.natcmp(b,a,true)
      end

      if matching_tags.length > 0
        last_tag = matching_tags[0]
      end
      return last_tag
    end

    def last_devqa_tag()
      return last_tag_matching('devqa-*')
    end

    def last_production_tag()
      return last_tag_matching('production-*')
    end

    task :calculate_tag do
      sh "git fetch"
            
      # ignore the tag-based deployment process and a deploy non-master branch
      if branch != "master" && stage == :devqa
        sh 'git push'
      else
        method = "tag_#{stage}"
        send method

        # push tags and latest code
        sh 'git push'
        sh 'git push --tags'
      end
    end

    desc "Mark the current code as a devqa release"
    task :tag_devqa do
      # find latest devqa tag for today
      new_tag_date = Date.today.to_s
      new_tag_serial = 1

      last_devqa_tag = last_tag_matching("devqa-#{new_tag_date}.*")
      if last_devqa_tag
        # calculate largest serial and increment
        last_devqa_tag =~ /devqa-[0-9]{4}-[0-9]{2}-[0-9]{2}\.([0-9]*)/
        new_tag_serial = $1.to_i + 1
      end

      new_devqa_tag = "devqa-#{new_tag_date}.#{new_tag_serial}"
      current_co_sha = `git log --pretty=format:%H HEAD -1`
      last_devqa_tag_sha = nil

      if last_devqa_tag
        last_devqa_tag_sha = `git log --pretty=format:%H #{last_devqa_tag} -1`
      end

      if last_devqa_tag_sha == current_co_sha
        puts "Not re-tagging devqa because the most recent tag (#{last_devqa_tag}) already points to current head"
        new_devqa_tag = last_devqa_tag
      else
        puts "Tagging current branch for deployment to devqa as '#{new_devqa_tag}'"
        sh "git tag -a -m 'tagging current code for deployment to devqa' #{new_devqa_tag}"
      end

      set :branch, new_devqa_tag
    end

    desc "Push the passed devqa tag to production. Pass in tag to deploy with '-s tag=devqa-YYYY-MM-DD.X'."
    task :tag_production do
      production_tag = configuration[:tag]
      if !production_tag && production_tag !~ /devqa-.*/
        puts "devqa tag required; use '-s tag=devqa-YYYY-MM-DD.X'"
        exit
      end

      raise "devqa tag #{production_tag} does not exist." unless last_tag_matching(production_tag)

      production_tag =~ /devqa-([0-9]{4}-[0-9]{2}-[0-9]{2}\.[0-9]*)/
      new_production_tag = "production-#{$1}"
      puts "promoting devqa tag #{production_tag} to production as '#{new_production_tag}'"
      sh "git tag -a -m 'tagging current code for deployment to production' #{new_production_tag} #{production_tag}"

      set :branch, new_production_tag
    end
  end
end
