# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_deployment_session',
  :secret      => '1b8fb33c32661d23a3930339ccaf9f75defe80c62ccc222a63e03bcf8017235b4e6c2d445650e9cbffb79613a561c53ee0bb4f2f2a7b384ca438d1780a71a93d'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
