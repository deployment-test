# require 'capistrano/ext/multistage'
# require 'config/git-deployment'
# 
# set :application, "deployment"
# set :repository,  "ssh://repo.or.cz/srv/git/deployment-test.git"
# set :scm, :git
# set :no_release, true
# 
# set :stages, %w(devqa production)
# set :default_stage, "devqa"
# set :branch, "master"
# 
# namespace :deploy do
#   task :start do; end
#   task :stop do; end
#   task :restart, :roles => :app, :except => { :no_release => true } do; end
# end

require 'grit'
require 'net/smtp'
require 'md5'
require 'cgi'

require 'capistrano/ext/multistage'
require 'config/git-deployment'

set :stages, %w(devqa production)
set :default_stage, "devqa"
set :scm, :git
set :application, "deployment"
set :repository, "ssh://repo.or.cz/srv/git/deployment-test.git"
set :git_shallow_clone, 1
set :deploy_via, :copy
set :copy_strategy, :export
set :copy_comperssion, :gzip

set :keep_releases, 4
set :group_writable, false

set :synchronous_connect, true
set :use_sudo, false

set :no_release, true # TODO

set :attachments_path, "attachments"
set :branch, begin branch rescue Grit::Repo.new(".").head.name || "master" end

after :symlink do
  # run "rm -r #{release_path}/#{attachments_path}"
  # run "ln -nfs #{shared_path}/#{attachments_path} #{release_path}/#{attachments_path}"
end

def send_mail(from, from_alias, to, to_alias, subject, message)
  msg = <<END_OF_MESSAGE
From: #{from_alias} <#{from}>
To: #{to_alias} <#{to}>
Subject: #{subject}
Content-Type: text/html; charset="utf-8"

#{message}
END_OF_MESSAGE

  Net::SMTP.start('mail.nodeta.fi', 25) do |smtp|
    smtp.send_message msg, from, to
  end
end

def get_branch
  run "touch #{branch_path}"
  return capture("cat #{branch_path}").chomp
end

def set_branch
  run "echo '#{_branch}' > #{branch_path}"
end

def get_deployed
  capture("cat #{deploy_to}/current/REVISION").to_s.chomp
end

namespace :deploy do
  desc "Restart mod_rails"
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "touch #{current_path}/tmp/restart.txt"
  end
  
  desc "Create attachments directory"
  after :setup, :roles => [:web, :app] do
    run "mkdir -p #{shared_path}/#{attachments_path}"
    run "mkdir -p tmp"
  end
  
  before :deploy do
    set :deployed, (get_deployed rescue nil)

    case stage
    when :production
      set :current_branch, "master"
      set :branch_path, "#{deploy_to}/current/BRANCH"
      set :branch,  "master" # force master on production
      set :_branch, "master"
    else
      set :branch_path, "#{deploy_to}/current/BRANCH"
      set :current_branch, (get_branch rescue "")
      set :_branch, "master"

      if current_branch != branch && !current_branch.empty?
        c = Capistrano::CLI.ui.ask <<-eos
\e[31mCurrently deployed branch is \e[1m\e[4m\e[33m#{current_branch}\e[0m\e[31m. You are deploying \e[1m\e[4m\e[33m#{branch}\e[0m\e[31m.\e[0m
\e[31m\e[1mProceed? (yes/no)\e[0m
eos

        exit unless c == "yes"
      end
    end
  end

  after :deploy do
    set_branch
  end
  
 # after :deploy, :"deploy:gems", :"deploy:minify", :"deploy:notify", :"deploy:cleanup", :"deploy:remove_test", :"deploy:run_at"
  
  desc "Remove test.html"
  task :remove_test do
    # run "cd #{current_path}/public && rm test.html"
  end
  
  desc "Run JavaScript and CSS optimizers"
  task :minify do
    # require 'config/initializers/static_files'
    # 
    # # Merge and minify CSS files
    # run "cd #{current_path}; ruby -S juicer merge --force -d ./public -o public/stylesheets/application.min.css #{CSS_FILES.map{|f| "./public/stylesheets/#{f}.css"}.join(" ")}"
    # # Merge and minify javascript files
    # run "cd #{current_path}; ruby -S juicer merge --force -s -i -m none -o public/javascripts/application.min.js #{JS_FILES.map{|f| "./public/javascripts/#{f}.js"}.join(" ")}"
  end
  
  desc "Notify flow about deployment using email"
  task "notify" do
    # repo = Grit::Repo.new(".")
    # repo.remote_fetch("origin")
    # 
    # if branch == current_branch
    #   message = "<p>The following changes were just deployed to #{host}:</p>"
    #   commits = repo.commits_between(deployed, repo.commit("origin/#{current_branch}").sha).reverse
    # 
    #   unless commits.empty?
    #     commits.each do |c|
    #       short, long = c.message.split(/\n+/, 2)
    # 
    #       message << "\n<div style=\"margin-bottom: 10px\"><div class=\"ui-corner-all\" style=\"background:url(http://gravatar.com/avatar/#{MD5::md5(c.author.email)}?s=30) no-repeat scroll center;height:30px;width:30px;float:left;margin-right:5px;\">&nbsp;</div>"
    #       message << "<div style=\"padding-left: 35px;\">#{CGI.escapeHTML(short)}<br/>"
    #       if long
    #         long.gsub!(/\n/, '<br />')
    #         message << '<p style="margin:5px 0px; padding: 0 5px; border-left: 3px solid #ccc">' + long + '</p>'
    #       end
    #       message << "<span style=\"font-size: 90%; color: #333\"><code>#{c.id_abbrev}</code> <a href=\"mailto:#{CGI.escapeHTML(c.author.email)}\">#{CGI.escapeHTML(c.author.name)}</a> on #{c.authored_date.strftime("%b %d, %H:%M")}</span></div></div>"
    #     end
    #   end
    # else
    #   message = "Branch #{branch} was deployed to #{host}. Previously deployed branch was #{current_branch}"
    # end
    # 
    # config = Grit::Config.new(repo)
    # send_mail(config["user.email"], config["user.name"], "flowdock@nodeta.tuntivirta.com", "Flowdock Team", "Flowdock Rails deployed #frontend #deploy ##{rails_env}", message)
  end
  
  desc "Update gems on remote server"
  task "gems" do
    # run "cd #{current_path}; RAILS_ENV=#{rails_env} rake gems:install"
  end
  desc "Run migrations on remote server"
  task "migrate" do
    # run "cd #{current_path}; RAILS_ENV=#{rails_env} rake db:migrate"
  end

  desc "Tell Integrity on Pollux to run acceptance tests"
  task "run_at" do
    # require "net/http"
    # Net::HTTP.new("gw.kutomotie.nodeta.fi", 880).start { |http|
    #   http.post("/flowdock-at/builds", nil)
    # }
  end
end

desc "List pending commits since last deploy"
task :undeployed do
  set :branch_path, "#{deploy_to}/current/BRANCH"
  set :deployed, get_deployed
  set :current_branch, get_branch
  `git fetch`
  puts

  if branch != current_branch
    HighLine.new.say <<-eos
Currently deployed branch is <%= color('#{current_branch.empty? ? 'unknown' : current_branch}', YELLOW+ON_RED) %>. Listing changes that would take effect when deploying differing branch <%= color('#{branch}', YELLOW+ON_RED) %> does not provide useful information.

Use '-s branch=<branch_name>' in command to compare non-master branch.
eos
    exit
  end

  Capistrano::CLI.ui.ask <<-eos
Should you deploy <%= color('#{branch}', YELLOW+ON_RED) %> to <%= color('#{stage}', YELLOW+BOLD+UNDERLINE) %> (current branch: <%= color('#{current_branch.empty? ? 'unknown' : current_branch}', YELLOW+ON_RED) %>),
the following changes would take effect.

(Press any key to continue)
  eos

  exec("git log #{deployed}..origin/#{branch}")
end
